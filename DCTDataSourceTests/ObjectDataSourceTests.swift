
import XCTest
import DCTDataSource

class ObjectDataSourceTests: XCTestCase {

	func testDepth() {
		let objectDataSource = ObjectDataSource(object: "object")
		let indexPathDepth = objectDataSource.indexPathDepth
		XCTAssert(indexPathDepth == 1, "\(indexPathDepth) is not correct.")
	}

	func testNumberOfItems() {
		let objectDataSource = ObjectDataSource(object: "object")
		let numberOfItems = objectDataSource.numberOfItems(indexPath: NSIndexPath(index: 0))
		XCTAssert(numberOfItems == 1, "\(numberOfItems) is not correct.")
	}

	func testEqual() {
		let objectDataSource = ObjectDataSource(object: "object")
		XCTAssert(objectDataSource == objectDataSource, "Should be equal.")
	}

	func testNotEqual() {
		let objectDataSource1 = ObjectDataSource(object: "object")
		let objectDataSource2 = ObjectDataSource(object: "somethingelse")
		XCTAssert(objectDataSource1 != objectDataSource2, "Should not be equal.")
	}

	func testObject() {
		let objectDataSource = ObjectDataSource(object: "object")
		let object = objectDataSource.objectAtIndexPath(NSIndexPath(index: 0))
		XCTAssert(object == "object", "\(object) is not correct.")
    }

	func testNotObject() {
		let objectDataSource = ObjectDataSource(object: "object")
		let object = objectDataSource.objectAtIndexPath(NSIndexPath(index: 1))
		XCTAssert(object == nil, "Should not have an object returned.")
	}

	func testObjectWithLongIndexPath() {
		let objectDataSource = ObjectDataSource(object: "object")
		let object = objectDataSource.objectAtIndexPath(NSIndexPath(indexes: [0, 0], length: 2))
		XCTAssert(object ==  nil, "Should not have an object returned.")
	}

	func testIndexPath() {
		let objectDataSource = ObjectDataSource(object: "object")
		let indexPath = objectDataSource.indexPathOfObject("object")
		XCTAssert(indexPath ==  NSIndexPath(index: 0), "\(indexPath) is not correct.")
	}

	func testNotIndexPath() {
		let objectDataSource = ObjectDataSource(object: "object")
		let indexPath = objectDataSource.indexPathOfObject(object: "nope")
		XCTAssert(indexPath ==  nil, "Should not have an index path returned.")
	}
}
