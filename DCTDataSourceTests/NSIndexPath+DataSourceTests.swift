
import XCTest
import DCTDataSource

class NSIndexPathDataSourceTests: XCTestCase {

	func testStartSubRange() {
		let indexPath = NSIndexPath(indexes: [0, 1, 2, 3, 4, 5], length: 6)
		let rangeIndexPath = indexPath[Range(start: 0, end: 1)]
		XCTAssert(rangeIndexPath != nil, "Should get an index path.")
		XCTAssert(rangeIndexPath!.length == 2, "\(rangeIndexPath): Length should be 2.")
		XCTAssert(rangeIndexPath!.indexAtPosition(0) == 0, "\(rangeIndexPath): Index should be 0.")
		XCTAssert(rangeIndexPath!.indexAtPosition(1) == 1, "\(rangeIndexPath): Index should be 1.")
	}

    func testMiddleSubRange() {
		let indexPath = NSIndexPath(indexes: [0, 1, 2, 3, 4, 5], length: 6)
		let rangeIndexPath = indexPath[Range(start: 2, end: 4)]
		XCTAssert(rangeIndexPath != nil, "Should get an index path.")
		XCTAssert(rangeIndexPath!.length == 3, "\(rangeIndexPath): Length should be 3.")
		XCTAssert(rangeIndexPath!.indexAtPosition(0) == 2, "\(rangeIndexPath): Index should be 2.")
		XCTAssert(rangeIndexPath!.indexAtPosition(1) == 3, "\(rangeIndexPath): Index should be 3.")
		XCTAssert(rangeIndexPath!.indexAtPosition(2) == 4, "\(rangeIndexPath): Index should be 4.")
    }

	func testEndSubRange() {
		let indexPath = NSIndexPath(indexes: [0, 1, 2, 3, 4, 5], length: 6)
		let rangeIndexPath = indexPath[Range(start: 4, end: 5)]
		XCTAssert(rangeIndexPath != nil, "Should get an index path.")
		XCTAssert(rangeIndexPath!.length == 2, "\(rangeIndexPath): Length should be 2.")
		XCTAssert(rangeIndexPath!.indexAtPosition(0) == 4, "\(rangeIndexPath): Index should be 4.")
		XCTAssert(rangeIndexPath!.indexAtPosition(1) == 5, "\(rangeIndexPath): Index should be 5.")
	}

}
