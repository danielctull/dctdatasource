
import Foundation

// Represent the updates that can be passed up the chain

public enum Update {
	case Delete(NSIndexPath)
	case Insert(NSIndexPath)
	case Reload(NSIndexPath)
	case Move(NSIndexPath, NSIndexPath)
}
