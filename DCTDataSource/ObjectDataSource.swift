
import Foundation

public class ObjectDataSource<ObjectType: Equatable>: DataSource {

//	weak var parentDataSource: DataSource?
	var object: ObjectType
	public init(object: ObjectType) {
		self.object = object
	}

	public var indexPathDepth: Int { return 1 }

	public func numberOfItems(indexPath indexPath: NSIndexPath) -> Int {
		assert(indexPath.length == 1, "IndexPath should be one in length")
		assert(indexPath.indexAtPosition(0) == 0, "Index should be zero")
		return 1
	}

	public func indexPathOfObject(object: ObjectType) -> NSIndexPath? {

		if object == self.object {
			return NSIndexPath(index: 0)
		}

		return nil
	}

	public func objectAtIndexPath(indexPath: NSIndexPath) -> ObjectType? {

		if indexPath.length != 1 {
			return nil
		}

		if indexPath.indexAtPosition(0) != 0 {
			return nil
		}

		return object
	}
}

public func ==<T>(lhs: ObjectDataSource<T>, rhs: ObjectDataSource<T>) -> Bool {
	return lhs.object == rhs.object
}
