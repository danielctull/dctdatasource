
import Foundation

public class SplitDataSource<ObjectType: Equatable, ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>: ParentDataSource {

	private var childDataSources: [ChildDataSource] = []

	public var indexPathDepth: Int { return 1 }


	// MARK: ParentDataSource


	// ATTENTION!!
	// -----------
	// The warnings below are, I suppose, because ChildDataSource is redefined in the generic parameter 
	// and it doesn't match the ChildDataSource in the array above.


	public func dataSourceAtIndexPath<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(indexPath: NSIndexPath) -> ChildDataSource? {
		assert(indexPath.length == 1, "IndexPath should be one in length")
		let index = indexPath.indexAtPosition(0)
		return childDataSources[index]
	}

	public func indexPathOfDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource) -> NSIndexPath? {

		guard let index = childDataSources.indexOf(dataSource) else {
			return nil
		}

		return NSIndexPath(index: index)
	}

	public func addChildDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource) {
		childDataSources.append(dataSource)
	}

	public func removeChildDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource) {

		guard let index = childDataSources.indexOf(dataSource) else {
			return
		}

		childDataSources.removeAtIndex(index)
	}

	// MARK: DataSource

	public func numberOfItems(indexPath indexPath: NSIndexPath) -> Int {
		assert(indexPath.length == 1, "IndexPath should be one in length")
		assert(indexPath.indexAtPosition(0) == 0, "Index should be zero")
		return childDataSources.count
	}

	public func indexPathOfObject(object: ObjectType) -> NSIndexPath? {
		return nil
	}
}

public func ==<T,U>(lhs: SplitDataSource<T,U>, rhs: SplitDataSource<T,U>) -> Bool {
	return lhs.childDataSources == rhs.childDataSources
}
