
public protocol ParentDataSource: DataSource {

	func dataSourceAtIndexPath<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(indexPath: NSIndexPath) -> ChildDataSource?
	func indexPathOfDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource) -> NSIndexPath?

	func addChildDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource)
	func removeChildDataSource<ChildDataSource: DataSource where ChildDataSource.ObjectType == ObjectType>(dataSource: ChildDataSource)
}




public extension ParentDataSource {

	// Default DataSource implementations

	public func objectAtIndexPath(indexPath: NSIndexPath) -> ObjectType? {

		// Given an indexPath of [1, 2], we want to get the child data source of the first part of the index path
		// while returning the object at the latter part. 
		//
		// If the indexPathDepth of the parent is 1, then we take the first part [1] and get the child data source
		// Use the second part [2] to get the object from the child.
		//
		// It is entirely possibly that the datasource hierarcy can be bigger than two index paths, for example
		// when using the NSOutlineView on the Mac. In this case you may have a given index path of [1, 2, 3, 4]
		// If this parent has a depth of 1, then we use the first part [1] to get the child data source and 
		// use the second part [2, 3, 4] to pass to the child data source, which in this case is likely to be 
		// another parent data source.

		let startRange = Range(start: 0, end: indexPathDepth - 1)
		guard let dataSourceIndexPath = indexPath[startRange] else {
			return nil
		}

		// NO IDEA ABOUT THIS - DT
		guard let dataSource = dataSourceAtIndexPath(dataSourceIndexPath) else {
			return nil
		}

		let endRange = Range(start: indexPathDepth, end: indexPath.length - 1)
		guard let objectIndexPath = indexPath[endRange] else {
			return nil
		}

		return dataSource.objectAtIndexPath(objectIndexPath)
	}
}
