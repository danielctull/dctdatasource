
import Foundation

public extension NSIndexPath {

	// Allows us to split NSIndexPaths
	public subscript (subrange: Range<Int>) -> NSIndexPath? {

		guard subrange.startIndex >= 0 else {
			return nil
		}

		let newLength = subrange.endIndex + 1 - subrange.startIndex
		guard newLength > 0 else {
			return nil
		}

		guard subrange.endIndex < length else {
			return nil
		}

		let indexes: UnsafeMutablePointer<Int> = UnsafeMutablePointer.alloc(newLength)
		getIndexes(indexes, range: NSRange(location: subrange.startIndex, length: newLength))
		return NSIndexPath(indexes:indexes, length:newLength)
	}
}
