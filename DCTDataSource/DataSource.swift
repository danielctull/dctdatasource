
import Foundation

public protocol DataSource: class, Equatable {

	typealias ObjectType: Equatable

	var indexPathDepth: Int { get }

	func numberOfItems(indexPath indexPath: NSIndexPath) -> Int
	func objectAtIndexPath(indexPath: NSIndexPath) -> ObjectType?
	func indexPathOfObject(object: ObjectType) -> NSIndexPath?

//	weak var parentDataSource: ParentDataSource? { get set }


	// Generally shouldn't need to implement these in a 
	// subclass other than to handle updates to the UI

    func reload()
    func beginUpdates()
    func perform(update: Update)
    func endUpdates()
}







public extension DataSource {

	// Default implementations for update handling

    func reload() {
//        if let parentDataSource = parentDataSource {
//            parentDataSource.reload()
//        }
    }

    func beginUpdates() {
//        if let parentDataSource = parentDataSource {
//            parentDataSource.beginUpdates()
//        }
    }

    func perform(update: Update) {
//        if let parentDataSource = parentDataSource {
//            parentDataSource.perform(update)
//        }
    }

    func endUpdates() {
//        if let parentDataSource = parentDataSource {
//            parentDataSource.endUpdates()
//        }
    }
}
